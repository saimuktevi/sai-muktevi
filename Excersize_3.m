% I am importing both text files
s = readtable ('test.txt');
f = readtable('IMU.txt');

% I am converting both text files to arrays so that they will be easy to
% work with.
test = s{:,:};
IMU = f{:,:};

% Here I am recording the dimensions of both arrays, so that I can use them
% to iterate through the data.
[r,c] = size(test);
[fr,fc] = size(IMU);

% I am initializing and creating each of the six sub-plots
dir(1) = subplot(2,3,1);
dir(2) = subplot(2,3,2);
dir(3) = subplot(2,3,3);
dir(4) = subplot(2,3,4);
dir(5) = subplot(2,3,5);
dir(6) = subplot(2,3,6);

% This line of code is set so that once a data plot is set, its data points
% will not be disturbed.
set(dir,'Nextplot','add');

%This for loop does all the heavy work of iterating through and graphing
%the data.
 for i = 1:r
     % The j variable is to keep track of the trials and each individual
     % direction. The if statement below is organized for each of the six
     % dimensions.
     j = test(i,2);
    if j == 0
        
        % This for loop is repeated identically for each direction. It
        % first cycles through every row in the IMU file
        for a = 1:fr
            
            % Here, I convert microseconds in the IMU to the seconds in
            % thetest file
            d = IMU(a,1)/1000000;
            
            % Now, I am setting the range for each trial, while taking into
            % account Fix-time with test(i,4)
            if(d>(test(i,7)+test(i,4))&&d<test(i,8))
                x = sqrt((IMU(a,3))^2+(IMU(a,4))^2+(IMU(a,5))^2);
                y = sqrt((IMU(a,6))^2+(IMU(a,7))^2+(IMU(a,8))^2);
                
                % I am plotting each direction into its respective
                % sub-plot. I am using the hold on command in order to keep
                % the data points from being erased.
                hold on
                plot(dir(1),x,y,'.r')
            end  
        end
    elseif j == 90
        for a = 1:fr
            d = IMU(a,1)/1000000;
            if(d>(test(i,7)+test(i,4))&&d<test(i,8))
                x = sqrt((IMU(a,3))^2+(IMU(a,4))^2+(IMU(a,5))^2);
                y = sqrt((IMU(a,6))^2+(IMU(a,7))^2+(IMU(a,8))^2);
                hold on
                plot(dir(2),x,y,'.r')
            end
        end
    elseif j == 135
        for a = 1:fr
            d = IMU(a,1)/1000000;
            if(d>(test(i,7)+test(i,4))&&d<test(i,8))
                x = sqrt((IMU(a,3))^2+(IMU(a,4))^2+(IMU(a,5))^2);
                y = sqrt((IMU(a,6))^2+(IMU(a,7))^2+(IMU(a,8))^2);
                hold on
                plot(dir(3),x,y,'.r')
            end
        end
    elseif j == 180
        for a = 1:fr
            d = IMU(a,1)/1000000;
            if(d>(test(i,7)+test(i,4))&&d<test(i,8))
                x = sqrt((IMU(a,3))^2+(IMU(a,4))^2+(IMU(a,5))^2);
                y = sqrt((IMU(a,6))^2+(IMU(a,7))^2+(IMU(a,8))^2);
                hold on
                plot(dir(4),x,y,'.r')
            end
        end
    elseif j == 270
         for a = 1:fr
            d = IMU(a,1)/1000000;
            if(d>(test(i,7)+test(i,4))&&d<test(i,8))
                x = sqrt((IMU(a,3))^2+(IMU(a,4))^2+(IMU(a,5))^2);
                y = sqrt((IMU(a,6))^2+(IMU(a,7))^2+(IMU(a,8))^2);
                hold on
                plot(dir(5),x,y,'.r')
            end
        end
    elseif j == 315
         for a = 1:fr
            d = IMU(a,1)/1000000;
            if(d>(test(i,7)+test(i,4))&&d<test(i,8))
                x = sqrt((IMU(a,3))^2+(IMU(a,4))^2+(IMU(a,5))^2);
                y = sqrt((IMU(a,6))^2+(IMU(a,7))^2+(IMU(a,8))^2);
                hold on
                plot(dir(6),x,y,'.r')
            end
        end
    end
 end